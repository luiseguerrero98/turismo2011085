/**
 * Created by Luis on 06/05/2016.
 */
var ruta = require('express').Router();
module.exports = (function (app){
    var lugarturistico = require('../controllers/LugaresTuristicosController')(app);
    ruta.post('/lugaresturisticos',lugarturistico.add);
    ruta.get('/lugaresturisticos',lugarturistico.list);
    ruta.put('/lugaresturisticos',lugarturistico.edit);
    ruta.delete('/lugaresturisticos',lugarturistico.delete);
    ruta.get('/lugaresturisticos/:id',lugarturistico.lugarconcomentarios);

    var usuario = require('../controllers/UsuariosController')(app);
    ruta.post('/usuarios',usuario.add);
    ruta.get('/usuarios',usuario.list);
    ruta.put('/usuarios',usuario.edit);
    ruta.delete('/usuarios',usuario.delete);
    ruta.get('/usuarios/:id',usuario.listid());

    var habitacion = require('../controllers/HabitacionesController')(app);
    ruta.post('/habitaciones',habitacion.add);
    ruta.get('/habitaciones',habitacion.list);
    ruta.put('/habitaciones',habitacion.edit);
    ruta.delete('/habitaciones',habitacion.delete);
    ruta.get('/habitaciones/:id',habitacion.listid);

    var hotel = require('../controllers/HotelesController')(app);
    ruta.post('/hoteles',hotel.add);
    ruta.get('/hoteles',hotel.list);
    ruta.put('/hoteles',hotel.edit);
    ruta.delete('/hoteles',hotel.delete);
    ruta.get('/hoteles/:id',hotel.listid);

    var login = require('../controllers/LoginController')(app);
    ruta.post('/login',login.add);
    ruta.get('/login/:correo', login.comprobarCorreo);

    var reservacion = require('../controllers/ReservacionesController')(app);
    ruta.post('/reservaciones',reservacion.add);
    ruta.get('/reservaciones',reservacion.list);
    ruta.put('/reservaciones',reservacion.edit);
    ruta.delete('/reservaciones',reservacion.delete);
    ruta.get('/reservaciones/:id',reservacion.listid);

    var tipohabitacion = require('../controllers/TipoHabitacionController')(app);
    ruta.post('/tiposhabitaciones',tipohabitacion.add);
    ruta.get('/tiposhabitaciones',tipohabitacion.list);
    ruta.put('/tiposhabitaciones',tipohabitacion.edit);
    ruta.delete('/tiposhabitaciones',tipohabitacion.delete);
    ruta.get('/tiposhabitaciones/:id',tipohabitacion.listid());

    return ruta;
});