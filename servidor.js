(function(){
	var express=require('express');
	var bodyParser=require('body-parser');
	var morgan=require('morgan');
	var mysql=require('mysql');
	var Sequelize = require('sequelize');

	var sequelize = new Sequelize('turismoDB','root','',{
		host: 'localhost',
		dialect: 'mysql',
		pool: {
			max: 20,
			min: 0
		}
	});

	var Roles = sequelize.define('roles', {
		idRol: {type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true},
		rol: {type: Sequelize.STRING, allowNull: false}
	});

	var Departamentos = sequelize.define('departamentos',{
		idDepartamento: {type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true},
		nombre: {type: Sequelize.STRING, allowNull: false}
	});

	var LugaresTuristicos = sequelize.define('lugaresturisticos',{
		idLugar: {type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true},
		direccion: {type: Sequelize.STRING, allowNull: false},
		descripcion: {type :Sequelize.STRING, allowNull: false},
		idDepartamento: { type: Sequelize.INTEGER, foreignKey: true, references: {
			model: Departamentos,
			key: 'idDepartamento'
		}}

	});

	var Hoteles = sequelize.define('hoteles',{
		idHotel: {type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true},
		nombre: {type: Sequelize.STRING, allowNull: false},
		direccion: {type :Sequelize.STRING, allowNull: false},
		web: {type :Sequelize.STRING, allowNull: false},
		telefono: {type :Sequelize.INTEGER, allowNull: false},
		estrellas: {type :Sequelize.INTEGER, allowNull: false},
		piscina: {type: Sequelize.BOOLEAN, allowNull: false},
		wifi: {type: Sequelize.BOOLEAN, allowNull: false},
		mascotas: {type: Sequelize.BOOLEAN, allowNull: false},
		bar: {type: Sequelize.BOOLEAN, allowNull: false},
		idDepartamento: { type: Sequelize.INTEGER, foreignKey: true, references: {
			model: Departamentos,
			key: 'idDepartamento'
		}}
	});

	var TipoHabitacion = sequelize.define('tipohabitacion', {
		idTipoHabitacion: {type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true},
		cantidadHabitaciones: {type: Sequelize.INTEGER, allowNull: false},
		idHotel: { type: Sequelize.INTEGER, foreignKey: true, references: {
			model: Hoteles,
			key: 'idHotel'
		}}
	});

	var Habitaciones = sequelize.define('habitaciones',{
		idHabitacion: {type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true},
		nombre: {type: Sequelize.STRING, allowNull: false},
		tamanio: {type :Sequelize.INTEGER, allowNull: false},
		precioNoche: {type :Sequelize.INTEGER, allowNull: false},
		maxPersonas: {type :Sequelize.INTEGER, allowNull: false},
		numeroCamas: {type :Sequelize.INTEGER, allowNull: false},
		aireAcondicionado: {type: Sequelize.BOOLEAN, allowNull: false},
		television: {type: Sequelize.BOOLEAN, allowNull: false},
		miniBar: {type: Sequelize.BOOLEAN, allowNull: false},
		cocina: {type: Sequelize.BOOLEAN, allowNull: false},
		balcon: {type: Sequelize.BOOLEAN, allowNull: false},
		terraza: {type: Sequelize.BOOLEAN, allowNull: false},
		baniera: {type: Sequelize.BOOLEAN, allowNull: false},
		disponible: {type: Sequelize.BOOLEAN, allowNull: false},
		idTipoHabitacion: { type: Sequelize.INTEGER, foreignKey: true, references: {
			model: TipoHabitacion,
			key: 'idTipoHabitacion'
		}}
	});

	var Usuarios = sequelize.define('usuarios',{
		idUsuario: {type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true},
		nombre: {type: Sequelize.STRING, allowNull: false},
		correo: {type :Sequelize.STRING, allowNull: false},
		nick: {type: Sequelize.STRING, allowNull: false},
		contrasenia: {type :Sequelize.STRING, allowNull: false},
		idRol: { type: Sequelize.INTEGER, foreignKey: true, references: {
			model: Roles,
			key: 'idRol'
		}}
	});

	var Comentarios = sequelize.define('comentarios',{
		idComentario: {type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true},
		comentario: {type: Sequelize.STRING, allowNull: false},
		idUsuario: { type: Sequelize.INTEGER, foreignKey: true, references: {
			model: Usuarios,
			key: 'idUsuario'
		}},
		idLugar: { type: Sequelize.INTEGER, foreignKey: true, references: {
			model: LugaresTuristicos,
			key: 'idLugar'
		}}
	});

	var Reservaciones = sequelize.define('reservaciones',{
		idReservacion: {type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true},
		fechaReservacion: {type: Sequelize.DATE, allowNull: false},
		fechaEntrada: {type: Sequelize.DATE, allowNull: false},
		fechaSalida: {type: Sequelize.DATE, allowNull: false},
		idUsuario: { type: Sequelize.INTEGER, foreignKey: true, references: {
			model: Usuarios,
			key: 'idUsuario'
		}},
		idHabitacion: { type: Sequelize.INTEGER, foreignKey: true, references: {
			model: Habitaciones,
			key: 'idHabitacion'
		}}
	});

	var FotoHotel = sequelize.define('fotohotel',{
		idFotoHotel: {type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true},
		foto: {type: Sequelize.BLOB, allowNull: false},
		idHotel: { type: Sequelize.INTEGER, foreignKey: true, references: {
			model: Hoteles,
			key: 'idHotel'
		}}
	});

	var FotoHabitacion = sequelize.define('fotohabitacion',{
		idFotoHabitacion: {type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true},
		foto: {type: Sequelize.BLOB, allowNull: false},
		idHabitacion: { type: Sequelize.INTEGER,foreignKey: true, references: {
			model: Habitaciones,
			key: 'idHabitacion'
		}}
	});

	var FotoLugar = sequelize.define('fotolugar',{
		idFotoLugar: {type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true},
		foto: {type: Sequelize.BLOB, allowNull: false},
		idLugar: { type: Sequelize.INTEGER, foreignKey: true, references: {
			model: LugaresTuristicos,
			key: 'idLugar'
		}}
	});

	Departamentos.hasMany(Hoteles, {foreignKey: 'idHotel'});
	Hoteles.belongsTo(Departamentos, {foreignKey: 'idHotel'});

	Departamentos.hasMany(LugaresTuristicos, {foreignKey: 'idLugar'});
	LugaresTuristicos.belongsTo(Departamentos, {foreignKey: 'idLugar'});

	Hoteles.hasMany(TipoHabitacion, {foreignKey: 'idTipoHabitacion'});
	TipoHabitacion.belongsTo(Hoteles, {foreignKey: 'idTipoHabitacion'});

	TipoHabitacion.hasMany(Habitaciones, {foreignKey: 'idHabitacion'});
	Habitaciones.belongsTo(TipoHabitacion, {foreignKey: 'idHabitacion'});

	Roles.hasMany(Usuarios, {foreignKey: 'idUsuario'});
	Usuarios.belongsTo(Roles,{foreignKey: 'idUsuario'});

	Usuarios.hasMany(Comentarios, {foreignKey: 'idComentario'});
	Comentarios.belongsTo(Usuarios, {foreignKey: 'idComentario'});

	LugaresTuristicos.hasMany(Comentarios, {foreignKey: 'idComentario'});
	Comentarios.belongsTo(LugaresTuristicos, {foreignKey: 'idComentario'});

	Habitaciones.hasMany(Reservaciones, {foreignKey: 'idReservacion'});
	Reservaciones.belongsTo(Habitaciones, {foreignKey: 'idReservacion'});

	Usuarios.hasMany(Reservaciones, {foreignKey: 'idReservacion'});
	Reservaciones.belongsTo(Usuarios, {foreignKey: 'idReservacion'});

	Hoteles.hasMany(FotoHotel, {foreignKey: 'idFotoHotel'});
	FotoHotel.belongsTo(Hoteles, {foreignKey: 'idFotoHotel'});

	Habitaciones.hasMany(FotoHabitacion, {foreignKey: 'idFotoHabitacion'});
	FotoHabitacion.belongsTo(Habitaciones, {foreignKey: 'idFotoHabitacion'});

	LugaresTuristicos.hasMany(FotoLugar, {foreignKey: 'idFotoLugar'});
	FotoLugar.belongsTo(LugaresTuristicos, {foreignKey: 'idFotoLugar'});

	sequelize.sync({force: false});
	var puerto = 3001;
	var conf = require('./config');
	var app= express();
	app.use(bodyParser.urlencoded({
		extended: true
	}));
	app.use(bodyParser.json());
	app.use('/api/v1',require('./routes')(app));
	app.use(morgan('dev'));
	
	app.set('lugaresturisticos', LugaresTuristicos)
	app.set('usuarios', Usuarios);
	app.set('comentarios', Comentarios);
	app.set('fotoshabitacion', FotoHabitacion);
	app.set('fotoshotel', FotoHotel);
	app.set('fotoslugar', FotoLugar);
	app.set('habitaciones', Habitaciones);
	app.set('hoteles', Hoteles);
	app.set('reservaciones', Reservaciones);
	app.set('tipohabitaciones', TipoHabitacion);
	
	app.listen(puerto,function(){
		console.log("Servidor iniciado en el puerto: " + puerto);
		console.log("Debug del server: ");
	})
})();