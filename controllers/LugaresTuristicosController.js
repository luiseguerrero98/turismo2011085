/**
 * Created by Luis on 06/05/2016.
 */
module.exports = function(app){
    return{
        add:function(pet, res) {
            var LugaresTuristicos = app.get('lugaresturisticos');
            LugaresTuristicos.create({
                direccion: pet.body.direccion,
                descripcion: pet.body.descripcion,
                idDepartamento: pet.body.idDepartamento
            }).then(function(lugarturistico){
                res.json(lugarturistico);
            });
        },
        list:function (pet,res) {
            var LugaresTuristicos = app.get('lugaresturisticos');
            LugaresTuristicos.findAll().then(function(lugaressturisticos){
                res.json(lugaressturisticos);
            });
        },
        edit:function (pet,res){
            var LugaresTuristicos = app.get('lugaresturisticos');
            LugaresTuristicos.findById(pet.body.idLugar).then(function(lugarturistico){
                if(lugarturistico){
                    lugarturistico.updateAttributes({
                        direccion: pet.body.direccion,
                        descripcion: pet.body.descripcion,
                        idDepartamento: pet.body.idDepartamento
                    }).then(function(lugarturistico){
                        res.json(lugarturistico);
                    });
                }else {
                    res.status(404).send({message: 'Lugar Turistico no encontrado'});
                }
            });
        },
        delete:function (pet,res) {
            var LugaresTuristicos = app.get('lugaresturisticos');
            LugaresTuristicos.destroy({
                where: {
                    idLugar: pet.body.idLugar
                }
            }).then(function(lugarturistico){
                res.json(lugarturistico);
            });
        },
        listid:function (pet, res){
            var LugaresTuristicos = app.get('lugaresturisticos');
            LugaresTuristicos.find(pet.body.idLugar).then(function(lugarturistico){
                if(lugarturistico){
                    res.json(lugarturistico);
                } else {
                    res.status(404).send({message: 'Lugar Turistico no encontrado'});
                }
            });
        },
        lugarconcomentarios:function (pet,res) {
            var LugaresTuristicos = app.get('lugaresturisticos');
            var Comentarios = app.get('comentarios');
            LugaresTuristicos.find({
                where: {idLugar: pet.params.id}, include: [Comentarios]}).then(function(lugarturistico){
                    res.json(lugarturistico);
            });
        }

    }
};