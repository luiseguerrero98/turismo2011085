/**
 * Created by Luis on 06/05/2016.
 */
module.exports = function(app){
    return{
        add:function(pet, res) {
            var FotosHotel = app.get('fotoshotel');
            FotosHotel.create({
                foto: pet.body.foto,
                idHotel: pet.body.idHotel
            }).then(function(fotohotel){
                res.json(fotohotel);
            });
        },
        list:function (pet,res) {
            var FotosHotel = app.get('fotoshotel');
            FotosHotel.findAll().then(function(fotoshotel){
                res.json(fotoshotel);
            });
        },
        edit:function (pet,res){
            var FotosHotel = app.get('fotoshotel');
            FotosHotel.findById(pet.body.idFotoHotel).then(function(fotohotel){
                if(fotohotel){
                    fotohotel.updateAttributes({
                        foto: pet.body.foto,
                        idHotel: pet.body.idHotel
                    }).then(function(fotohotel){
                        res.json(fotohotel);
                    });
                }else {
                    res.status(404).send({message: 'Foto de Hotel no encontrada'});
                }
            });
        },
        delete:function (pet,res) {
            var FotosHotel = app.get('fotoshotel');
            FotosHotel.destroy({
                where: {
                    idFotoHotel: pet.body.idFotoHotel
                }
            }).then(function(fotohotel){
                res.json(fotohotel);
            });
        },
        listid:function (pet, res){
            var FotosHotel = app.get('fotoshotel');
            FotosHotel.find(pet.body.idFotoHotel).then(function(fotohotel){
                if(fotohotel){
                    res.json(fotohotel);
                } else {
                    res.status(404).send({message: 'Foto de Hotel no encontrada'});
                }
            });
        }

    }
};