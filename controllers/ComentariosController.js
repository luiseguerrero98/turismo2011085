/**
 * Created by Luis on 06/05/2016.
 */
module.exports = function(app){
    return{
        add:function(pet, res) {
            var Comentarios = app.get('comentarios');
            Comentarios.create({
                comentario: pet.body.comentario,
                idComentario: pet.body.idComentario,
                idLugar: pet.body.idLugar
            }).then(function(comentario){
                res.json(comentario);
            });
        },
        list:function (pet,res) {
            var Comentarios = app.get('comentarios');
            Comentarios.findAll().then(function(comentarios){
                res.json(comentarios);
            });
        },
        edit:function (pet,res){
            var Comentarios = app.get('comentarios');
            Comentarios.findById(pet.body.idComentario).then(function(comentario){
                if(comentario){
                    comentario.updateAttributes({
                        comentario: pet.body.comentario,
                        idComentario: pet.body.idComentario,
                        idLugar: pet.body.idLugar
                    }).then(function(comentario){
                        res.json(comentario);
                    });
                }else {
                    res.status(404).send({message: 'Comentario no encontrado'});
                }
            });
        },
        delete:function (pet,res) {
            var Comentarios = app.get('comentarios');
            Comentarios.destroy({
                where: {
                    idComentario: pet.body.idComentario
                }
            }).then(function(comentario){
                res.json(comentario);
            });
        },
        listid:function (pet, res){
            var Comentarios = app.get('comentarios');
            Comentarios.find(pet.body.idComentario).then(function(comentario){
                if(comentario){
                    res.json(comentario);
                } else {
                    res.status(404).send({message: 'Comentario no encontrado'});
                }
            });
        }

    }
};