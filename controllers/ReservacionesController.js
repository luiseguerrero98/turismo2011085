module.exports = function(app){
    return{
        add:function(pet, res) {
            var Reservaciones = app.get('reservaciones');
            Reservaciones.create({
                fechaReservacion: pet.body.fechaReservacion,
                fechaEntrada: pet.body.fechaEntrada,
                fechaSalida: pet.body.fechaSalida,
                idUsuario: pet.body.idUsuario
            }).then(function(reservacion){
                res.json(reservacion);
            });
        },
        list:function (pet,res) {
            var Reservaciones = app.get('reservaciones');
            Reservaciones.findAll().then(function(reservaciones){
                res.json(reservaciones);
            });
        },
        edit:function (pet,res){
            var Reservaciones = app.get('reservaciones');
            Reservaciones.findById(pet.body.idReservacion).then(function(reservacion){
                if(reservacion){
                    reservacion.updateAttributes({
                        fechaReservacion: pet.body.fechaReservacion,
                        fechaEntrada: pet.body.fechaEntrada,
                        fechaSalida: pet.body.fechaSalida,
                        idUsuario: pet.body.idUsuario
                    }).then(function(reservacion){
                        res.json(reservacion);
                    });
                }else {
                    res.status(404).send({message: 'Comentario no encontrado'});
                }
            });
        },
        delete:function (pet,res) {
            var Reservaciones = app.get('reservaciones');
            Reservaciones.destroy({
                where: {
                    idReservacion: pet.body.idReservacion
                }
            }).then(function(reservacion){
                res.json(reservacion);
            });
        },
        listid:function (pet, res){
            var Reservaciones = app.get('reservaciones');
            Reservaciones.find(pet.body.idReservacion).then(function(reservacion){
                if(reservacion){
                    res.json(reservacion);
                } else {
                    res.status(404).send({message: 'Comentario no encontrado'});
                }
            });
        }

    }
};