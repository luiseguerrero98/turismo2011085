/**
 * Created by Luis on 06/05/2016.
 */
module.exports = function(app){
    return{
        add:function(pet, res) {
            var TipoHabitaciones = app.get('tipohabitaciones');
            TipoHabitaciones.create({
                cantidadHabitaciones: pet.body.cantidadHabitaciones,
                idHotel: pet.body.idHotel
            }).then(function(usuario){
                res.json(usuario);
            });
        },
        list:function (pet,res) {
            var TipoHabitaciones = app.get('tipohabitaciones');
            TipoHabitaciones.findAll().then(function(tipohabitaciones){
                res.json(tipohabitaciones);
            });
        },
        edit:function (pet,res){
            var TipoHabitaciones = app.get('tipohabitaciones');
            TipoHabitaciones.findById(pet.body.idTipoHabitacion).then(function(usuario){
                if(usuario){
                    usuario.updateAttributes({
                        cantidadHabitaciones: pet.body.cantidadHabitaciones,
                        idHotel: pet.body.idHotel
                    }).then(function(usuario){
                        res.json(usuario);
                    });
                }else {
                    res.status(404).send({message: 'Tipo Habitacion no encontrado'});
                }
            });
        },
        delete:function (pet,res) {
            var TipoHabitaciones = app.get('tipohabitaciones');
            TipoHabitaciones.destroy({
                where: {
                    idTipoHabitacion: pet.body.idTipoHabitacion
                }
            }).then(function(usuario){
                res.json(usuario);
            });
        },
        listid:function (pet, res){
            var TipoHabitaciones = app.get('tipohabitaciones');
            TipoHabitaciones.find(pet.body.idTipoHabitacion).then(function(usuario){
                if(usuario){
                    res.json(usuario);
                } else {
                    res.status(404).send({message: 'Tipo Habitacion no encontrado'});
                }
            });
        }

    }
};