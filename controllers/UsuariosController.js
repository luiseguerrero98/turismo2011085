/**
 * Created by Luis on 06/05/2016.
 */
module.exports = function(app){
    return{
        add:function(pet, res) {
            var Usuarios = app.get('usuarios');
            Usuarios.create({
                nombre: pet.body.nombre,
                correo: pet.body.correo,
                nick: pet.body.nick,
                contrasenia: pet.body.contrasenia,
                idRol: pet.body.idRol
            }).then(function(usuario){
                res.json(usuario);
            });
        },
        list:function (pet,res) {
            var Usuarios = app.get('usuarios');
            Usuarios.findAll().then(function(usuarios){
                res.json(usuarios);
            });
        },
        edit:function (pet,res){
            var Usuarios = app.get('usuarios');
            Usuarios.findById(pet.body.idUsuario).then(function(usuario){
                if(usuario){
                    usuario.updateAttributes({
                        nombre: pet.body.nombre,
                        correo: pet.body.correo,
                        nick: pet.body.nick,
                        contrasenia: pet.body.contrasenia,
                        idRol: pet.body.idRol
                    }).then(function(usuario){
                        res.json(usuario);
                    });
                }else {
                    res.status(404).send({message: 'Usuario no encontrado'});
                }
            });
        },
        delete:function (pet,res) {
            var Usuarios = app.get('usuarios');
            Usuarios.destroy({
                where: {
                    idUsuario: pet.body.idUsuario
                }
            }).then(function(usuario){
                res.json(usuario);
            });
        },
        listid:function (pet, res){
            var Usuarios = app.get('usuarios');
            Usuarios.find(pet.body.idUsuario).then(function(usuario){
                if(usuario){
                    res.json(usuario);
                } else {
                    res.status(404).send({message: 'Usuario no encontrado'});
                }
            });
        }

    }
};