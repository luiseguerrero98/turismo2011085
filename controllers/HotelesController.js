/**
 * Created by Luis on 06/05/2016.
 */
module.exports = function(app){
    return{
        add:function(pet, res) {
            var Hoteles = app.get('hoteles');
            Hoteles.create({
                nombre: pet.body.nombre,
                direccion: pet.body.direccion,
                web: pet.body.web,
                telefono: pet.body.telefono,
                estrellas: pet.body.estrellas,
                piscina: pet.body.piscina,
                wifi: pet.body.wifi,
                mascotas: pet.body.mascotas,
                bar: pet.body.bar,
                idDepartamento: pet.body.idDepartamento
            }).then(function(hotel){
                res.json(hotel);
            });
        },
        list:function (pet,res) {
            var Hoteles = app.get('hoteles');
            Hoteles.findAll().then(function(hoteles){
                res.json(hoteles);
            });
        },
        edit:function (pet,res){
            var Hoteles = app.get('hoteles');
            Hoteles.findById(pet.body.idHotel).then(function(hotel){
                if(hotel){
                    hotel.updateAttributes({
                        nombre: pet.body.nombre,
                        direccion: pet.body.direccion,
                        web: pet.body.web,
                        telefono: pet.body.telefono,
                        estrellas: pet.body.estrellas,
                        piscina: pet.body.piscina,
                        wifi: pet.body.wifi,
                        mascotas: pet.body.mascotas,
                        bar: pet.body.bar,
                        idDepartamento: pet.body.idDepartamento
                    }).then(function(hotel){
                        res.json(hotel);
                    });
                }else {
                    res.status(404).send({message: 'Hotel no encontrado'});
                }
            });
        },
        delete:function (pet,res) {
            var Hoteles = app.get('hoteles');
            Hoteles.destroy({
                where: {
                    idHotel: pet.body.idHotel
                }
            }).then(function(hotel){
                res.json(hotel);
            });
        },
        listid:function (pet, res){
            var Hoteles = app.get('hoteles');
            Hoteles.find(pet.body.idHotel).then(function(hotel){
                if(hotel){
                    res.json(hotel);
                } else {
                    res.status(404).send({message: 'Hotel no encontrado'});
                }
            });
        }

    }
};