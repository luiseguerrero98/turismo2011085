/**
 * Created by Luis on 06/05/2016.
 */
module.exports = function(app){
    return{
        add:function(pet, res) {
            var FotosHabitacion = app.get('fotoshabitacion');
            FotosHabitacion.create({
                foto: pet.body.foto,
                idHabitacion: pet.body.idHabitacion
            }).then(function(fotohabitacion){
                res.json(fotohabitacion);
            });
        },
        list:function (pet,res) {
            var FotosHabitacion = app.get('fotoshabitacion');
            FotosHabitacion.findAll().then(function(fotoshabitacion){
                res.json(fotoshabitacion);
            });
        },
        edit:function (pet,res){
            var FotosHabitacion = app.get('fotoshabitacion');
            FotosHabitacion.findById(pet.body.idFotoHabitacion).then(function(fotohabitacion){
                if(fotohabitacion){
                    fotohabitacion.updateAttributes({
                        foto: pet.body.foto,
                        idHabitacion: pet.body.idHabitacion
                    }).then(function(fotohabitacion){
                        res.json(fotohabitacion);
                    });
                }else {
                    res.status(404).send({message: 'Foto de Habitacion no encontrada'});
                }
            });
        },
        delete:function (pet,res) {
            var FotosHabitacion = app.get('fotoshabitacion');
            FotosHabitacion.destroy({
                where: {
                    idFotoHabitacion: pet.body.idFotoHabitacion
                }
            }).then(function(fotohabitacion){
                res.json(fotohabitacion);
            });
        },
        listid:function (pet, res){
            var FotosHabitacion = app.get('fotoshabitacion');
            FotosHabitacion.find(pet.body.idFotoHabitacion).then(function(fotohabitacion){
                if(fotohabitacion){
                    res.json(fotohabitacion);
                } else {
                    res.status(404).send({message: 'Foto de Habitacion no encontrada'});
                }
            });
        }

    }
};