/**
 * Created by Luis on 06/05/2016.
 */
module.exports = function(app){
    return{
        add:function(pet, res) {
            var Habitaciones = app.get('habitaciones');
            Habitaciones.create({
                nombre: pet.body.nombre,
                tamanio: pet.body.tamanio,
                precioNoche: pet.body.precioNoche,
                maxPersonas: pet.body.maxPersonas,
                numeroCamas: pet.body.numeroCamas,
                aireAcondicionado: pet.body.aireAcondicionado,
                television: pet.body.television,
                miniBar: pet.body.miniBar,
                cocina: pet.body.cocina,
                balcon: pet.body.balcon,
                terraza: pet.body.terraza,
                baniera: pet.body.baniera,
                disponible: pet.body.disponible,
                idTipoHabitacion: pet.body.idTipoHabitacion
            }).then(function(habitacion){
                res.json(habitacion);
            });
        },
        list:function (pet,res) {
            var Habitaciones = app.get('habitaciones');
            Habitaciones.findAll().then(function(habitaciones){
                res.json(habitaciones);
            });
        },
        edit:function (pet,res){
            var Habitaciones = app.get('habitaciones');
            Habitaciones.findById(pet.body.idHabitacion).then(function(habitacion){
                if(habitacion){
                    habitacion.updateAttributes({
                        nombre: pet.body.nombre,
                        tamanio: pet.body.tamanio,
                        precioNoche: pet.body.precioNoche,
                        maxPersonas: pet.body.maxPersonas,
                        numeroCamas: pet.body.numeroCamas,
                        aireAcondicionado: pet.body.aireAcondicionado,
                        television: pet.body.television,
                        miniBar: pet.body.miniBar,
                        cocina: pet.body.cocina,
                        balcon: pet.body.balcon,
                        terraza: pet.body.terraza,
                        baniera: pet.body.baniera,
                        disponible: pet.body.disponible,
                        idTipoHabitacion: pet.body.idTipoHabitacion
                    }).then(function(habitacion){
                        res.json(habitacion);
                    });
                }else {
                    res.status(404).send({message: 'Lugar Turistico no encontrado'});
                }
            });
        },
        delete:function (pet,res) {
            var Habitaciones = app.get('habitaciones');
            Habitaciones.destroy({
                where: {
                    idHabitacion: pet.body.idHabitacion
                }
            }).then(function(habitacion){
                res.json(habitacion);
            });
        },
        listid:function (pet, res){
            var Habitaciones = app.get('habitaciones');
            Habitaciones.find(pet.body.idHabitacion).then(function(habitacion){
                if(habitacion){
                    res.json(habitacion);
                } else {
                    res.status(404).send({message: 'Lugar Turistico no encontrado'});
                }
            });
        },
        lugarconcomentarios:function (pet,res) {
            var Habitaciones = app.get('habitaciones');
            var Comentarios = app.get('comentarios');
            Habitaciones.find({
                where: {idHabitacion: pet.params.id}, include: [Comentarios]}).then(function(habitacion){
                res.json(habitacion);
            });
        }

    }
};