/**
 * Created by Luis on 06/05/2016.
 */
module.exports = function(app){
    return{
        add:function(pet, res) {
            var FotosLugar = app.get('fotoslugar');
            FotosLugar.create({
                foto: pet.body.foto,
                idLugar: pet.body.idLugar
            }).then(function(fotolugar){
                res.json(fotolugar);
            });
        },
        list:function (pet,res) {
            var FotosLugar = app.get('fotoslugar');
            FotosLugar.findAll().then(function(fotoslugar){
                res.json(fotoslugar);
            });
        },
        edit:function (pet,res){
            var FotosLugar = app.get('fotoslugar');
            FotosLugar.findById(pet.body.idFotoLugar).then(function(fotolugar){
                if(fotolugar){
                    fotolugar.updateAttributes({
                        foto: pet.body.foto,
                        idLugar: pet.body.idLugar
                    }).then(function(fotolugar){
                        res.json(fotolugar);
                    });
                }else {
                    res.status(404).send({message: 'Foto de Lugar no encontrada'});
                }
            });
        },
        delete:function (pet,res) {
            var FotosLugar = app.get('fotoslugar');
            FotosLugar.destroy({
                where: {
                    idFotoLugar: pet.body.idFotoLugar
                }
            }).then(function(fotolugar){
                res.json(fotolugar);
            });
        },
        listid:function (pet, res){
            var FotosLugar = app.get('fotoslugar');
            FotosLugar.find(pet.body.idFotoLugar).then(function(fotolugar){
                if(fotolugar){
                    res.json(fotolugar);
                } else {
                    res.status(404).send({message: 'Foto de Lugar no encontrada'});
                }
            });
        }

    }
};